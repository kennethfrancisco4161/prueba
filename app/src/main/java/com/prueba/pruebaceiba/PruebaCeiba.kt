package com.prueba.pruebaceiba

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class PruebaCeiba: Application() {
}