package com.prueba.pruebaceiba.di

import android.content.Context
import androidx.room.Room
import com.prueba.pruebaceiba.database.DataBase
import com.prueba.pruebaceiba.database.dao.PostDao
import com.prueba.pruebaceiba.database.dao.UserDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object RoomModule {

    @Singleton
    @Provides
    fun provideAppDB(@ApplicationContext context: Context): DataBase {
        return Room
            .databaseBuilder(
                context,
                DataBase::class.java,
                DataBase.DATABASE_NAME
            )
            .fallbackToDestructiveMigration()
            .build()
    }


    @Singleton
    @Provides
    fun provideuserDao(appDatabase: DataBase): UserDao {
        return appDatabase.userDao()
    }

    @Singleton
    @Provides
    fun providePostDao(appDatabase: DataBase): PostDao {
        return appDatabase.postDao()
    }

}