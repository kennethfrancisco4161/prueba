package com.prueba.pruebaceiba.network

import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.network.response.PostsResponse
import com.prueba.pruebaceiba.network.response.UserResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface APIClientService {

    @GET("/users")
    suspend fun getUsers(): ArrayList<UserResponse>

    @GET("/posts")
    suspend fun getPosts(): ArrayList<PostsResponse>

    @GET("/posts/{userId}")
    suspend fun getPostsById(@Path("userId") userId: Int): List<PostsRoomEntity>

}