package com.prueba.pruebaceiba.network.response

data class PostsResponse(
    var userId: Int,
    var id: Int,
    var title: String,
    var body: String
)
