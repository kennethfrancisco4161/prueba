package com.prueba.pruebaceiba.network.response

import com.prueba.pruebaceiba.model.Address
import com.prueba.pruebaceiba.model.Company

data class UserResponse(
    var id: Int,
    var name : String,
    var username : String,
    var email : String,
    var address : Address,
    var phone : String,
    var website : String,
    var company : Company
)
