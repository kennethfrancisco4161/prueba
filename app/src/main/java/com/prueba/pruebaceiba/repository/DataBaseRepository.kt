package com.prueba.pruebaceiba.repository

import com.prueba.pruebaceiba.database.dao.PostDao
import com.prueba.pruebaceiba.database.dao.UserDao
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.database.entities.UserRoomEntity
import com.prueba.pruebaceiba.network.APIClientService
import javax.inject.Inject

class DataBaseRepository @Inject constructor(
    private val service: APIClientService,
    private val userDao: UserDao,
    private val postDao: PostDao
){

    suspend fun getAllUsers() = service.getUsers()

    suspend fun getAllPosts() = service.getPosts()

    suspend fun getPostsById(userId: Int) = service.getPostsById(userId)

    suspend fun saveAllUser(entity: List<UserRoomEntity>) {
        userDao.updateData(entity)
    }

    suspend fun saveAllPosts(entity: List<PostsRoomEntity>) {
        postDao.updateData(entity)
    }

    suspend fun getAllUserBd(): List<UserRoomEntity> {
        return userDao.getAllData()
    }

    fun getUserByProcessId(id: Long) = userDao.getUserByProcessId(id)

    fun getPostsById(id: Long) = postDao.getUserByProcessId(id)

}