package com.prueba.pruebaceiba.model

data class Geo(
    var lat : String,
    var lng : String
)
