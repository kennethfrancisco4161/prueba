package com.prueba.pruebaceiba.ui.home.detail

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.prueba.pruebaceiba.base.BaseFragment
import com.prueba.pruebaceiba.base.adapter.PostsAdapter
import com.prueba.pruebaceiba.databinding.FragmentDetailBinding
import com.prueba.pruebaceiba.ui.home.hm.HomeViewModel

class DetailFragment : BaseFragment(
    title = "Prueba de Ingreso"
) {

    private val viewModel: HomeViewModel  by activityViewModels()
    private lateinit var binding : FragmentDetailBinding

    private val args : DetailFragmentArgs by navArgs()

    private lateinit var postsAdapter: PostsAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =  FragmentDetailBinding.inflate(inflater, container, false)
        loadForm(args.userId.toLong())

        binding.apply {

            postsAdapter = PostsAdapter()
            listPublish.layoutManager = LinearLayoutManager(requireContext())
            listPublish.adapter = postsAdapter

        }

        return binding.root
    }

    private fun loadForm(id: Long) {
        viewModel.getUserById(id).observe(viewLifecycleOwner){
            if(it != null){
                binding.apply {
                    nameTitle.text = it.name
                    emailTitle.text = it.email
                    phoneTitle.text = it.phone
                }
            }
        }
        viewModel.getPostById(id).observe(viewLifecycleOwner){
            if(it != null){
                postsAdapter.submitList(it)
            }
        }
    }

}