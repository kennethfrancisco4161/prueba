package com.prueba.pruebaceiba.ui.home.hm

import android.util.Log
import androidx.lifecycle.*
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.database.entities.UserRoomEntity
import com.prueba.pruebaceiba.network.response.PostsResponse
import com.prueba.pruebaceiba.network.response.Resource
import com.prueba.pruebaceiba.network.response.UserResponse
import com.prueba.pruebaceiba.repository.DataBaseRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(
    private val repository: DataBaseRepository
) : ViewModel() {

    private var _usersLiveData: MutableLiveData<ArrayList<UserResponse>> = MutableLiveData()
    val usersLiveData: LiveData<ArrayList<UserResponse>>
        get() =  _usersLiveData

    private var _postsLiveData: MutableLiveData<ArrayList<PostsResponse>> = MutableLiveData()
    val postsLiveData: LiveData<ArrayList<PostsResponse>>
        get() =  _postsLiveData

    fun getUsers() = liveData {
        emit(Resource.loading(null))
        try {
            val users = repository.getAllUsers()
            _usersLiveData.postValue(users)

            val items : MutableList<UserRoomEntity> = mutableListOf()
            for(i in users) {
                val user = UserRoomEntity(
                    i.id,
                    i.name,
                    i.username,
                    i.email,
                    i.address,
                    i.phone,
                    i.website,
                    i.company
                )
                items.add(user)
            }
            repository.saveAllUser(items)
            emit(Resource.success(true))

        } catch (exception: Exception) {
            Log.e("error", exception.toString())
            emit(Resource.error(data = null, msg = exception.message ?: "Error Occurred!"))
        }
    }

    fun getPosts() = liveData {
        emit(Resource.loading(null))
        try {
            val posts = repository.getAllPosts()
            _postsLiveData.postValue(posts)

            val items : MutableList<PostsRoomEntity> = mutableListOf()

            for(i in posts){
                val post = PostsRoomEntity(
                    i.id,
                    i.userId,
                    i.title,
                    i.body
                )
                items.add(post)
            }
            repository.saveAllPosts(items)
            emit(Resource.success(true))

        } catch (exception: Exception) {
            Log.e("error", exception.toString())
            emit(Resource.error(data = null, msg = exception.message ?: "Error Occurred!"))
        }
    }


    fun getAllUsersBd() =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(repository.getAllUserBd())
        }

    fun getUserById(id: Long) =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(repository.getUserByProcessId(id))
        }

    fun getPostById(id: Long) =
        liveData(context = viewModelScope.coroutineContext + Dispatchers.IO) {
            emit(repository.getPostsById(id))
        }
}