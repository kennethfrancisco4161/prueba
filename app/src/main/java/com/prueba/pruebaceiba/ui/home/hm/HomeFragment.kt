package com.prueba.pruebaceiba.ui.home.hm

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.prueba.pruebaceiba.base.BaseFragment
import com.prueba.pruebaceiba.base.adapter.UserAdapter
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.database.entities.UserRoomEntity
import com.prueba.pruebaceiba.databinding.FragmentHomeBinding
import com.prueba.pruebaceiba.network.response.Resource

class HomeFragment : BaseFragment(
    title = "Prueba de Ingreso",
    isHome = true
) {

    private val viewModel: HomeViewModel by viewModels()
    private lateinit var binding: FragmentHomeBinding

    private lateinit var userAdapter: UserAdapter

    private var userRoomEntity : List<UserRoomEntity> = listOf()
    private lateinit var userRoomEntityListFilter : List<UserRoomEntity>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentHomeBinding.inflate(inflater, container, false)

        binding.apply {

            userAdapter = UserAdapter{
                detailForm(it)
            }

            listUsers.layoutManager = LinearLayoutManager(requireContext())
            listUsers.adapter = userAdapter

            findUsers.addTextChangedListener { filtro ->
                userRoomEntityListFilter = userRoomEntity.filter { user -> user.name!!.lowercase().contains(filtro.toString().lowercase())}
                userAdapter.submitList(userRoomEntityListFilter)
                userAdapter.notifyDataSetChanged()
            }
        }

       if(userRoomEntity.isNotEmpty()){
           addObserver()
       }else{
           getUsers()
       }

        getPosts()


        return binding.root
    }

    private fun detailForm(it: UserRoomEntity) {
        val action = HomeFragmentDirections.actionHomeFragmentToDetailFragment(it.id)
        findNavController().navigate(action)
    }

    private fun getUsers() {
        viewModel.getUsers().observe(viewLifecycleOwner){
            when(it.status){
                Resource.Status.LOADING ->{
                    loadingDialog.startLoadingDialog()
                }
                Resource.Status.SUCCESS ->{
                    addObserver()
                    loadingDialog.dismissDialog()
                }
                Resource.Status.ERROR->{
                    Toast.makeText(requireContext(), "ERRRRRROR!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun getPosts(){
        viewModel.getPosts().observe(viewLifecycleOwner){
            when(it.status){
                Resource.Status.LOADING ->{
                    Log.i("Carganado", "esta cargando...")
                }
                Resource.Status.SUCCESS ->{
                    Log.i("Con exito", "Se guradaron los datos en la bd!")
                }
                Resource.Status.ERROR->{
                    Toast.makeText(requireContext(), "ERRRRRROR!", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun addObserver(){
        viewModel.usersLiveData.observe(viewLifecycleOwner){objc ->
            objc.forEach{
                println(it.username)
            }
        }
        viewModel.getAllUsersBd().observe(viewLifecycleOwner){
            if(it != null){
                userRoomEntity = it
                userAdapter.submitList(it)
            }
        }

    }

}