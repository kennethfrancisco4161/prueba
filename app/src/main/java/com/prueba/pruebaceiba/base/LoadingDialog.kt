package com.prueba.pruebaceiba.base

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import androidx.core.content.ContextCompat
import com.prueba.pruebaceiba.R

class LoadingDialog(
    val context: Context
) {
    private lateinit var dialog: Dialog

    @SuppressLint("InflateParams")
    fun startLoadingDialog() {
        val builder = AlertDialog.Builder(this.context)
        builder.setCancelable(false)
        val inflater = LayoutInflater.from(context)
        builder.setView(inflater.inflate(R.layout.dialog_loading, null))

        dialog = builder.create().apply {
            val background = ContextCompat.getDrawable(context, R.drawable.background_dialog)
            window?.setBackgroundDrawable(background)
        }
        dialog.show()
        val width = context.resources.getDimensionPixelSize(R.dimen.dialog)
        dialog.window!!.setLayout(width, width)
    }

    fun dismissDialog() {
        dialog.dismiss()
    }

}