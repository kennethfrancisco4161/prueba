package com.prueba.pruebaceiba.base.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.prueba.pruebaceiba.R
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity

class PostsAdapter (
): ListAdapter<PostsRoomEntity, PostsAdapter.ViewHolder>(object: DiffUtil.ItemCallback<PostsRoomEntity>(){
    override fun areItemsTheSame(oldItem: PostsRoomEntity, newItem: PostsRoomEntity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: PostsRoomEntity, newItem: PostsRoomEntity): Boolean {
        return oldItem.id == newItem.id
    }
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_posts, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title : TextView = view.findViewById(R.id.title)
        val description : TextView = view.findViewById(R.id.description)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val post = getItem(position)
        holder.title.text = post.title
        holder.description.text = post.body

    }

}