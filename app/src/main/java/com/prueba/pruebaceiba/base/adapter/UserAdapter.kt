package com.prueba.pruebaceiba.base.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.prueba.pruebaceiba.R
import com.prueba.pruebaceiba.database.entities.UserRoomEntity

class UserAdapter(
    val onItemSelected: (form: UserRoomEntity) -> (Unit)
): ListAdapter<UserRoomEntity, UserAdapter.ViewHolder>(object: DiffUtil.ItemCallback<UserRoomEntity>(){
    override fun areItemsTheSame(oldItem: UserRoomEntity, newItem: UserRoomEntity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: UserRoomEntity, newItem: UserRoomEntity): Boolean {
        return oldItem.id == newItem.id
    }
}) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_form, parent, false)
        return ViewHolder(view)
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val name : TextView = view.findViewById(R.id.name)
        val number : TextView = view.findViewById(R.id.number)
        val email : TextView = view.findViewById(R.id.email)
        val publish : TextView = view.findViewById(R.id.seePublish)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val user = getItem(position)
        holder.name.text = user.name
        holder.number.text = user.phone
        holder.email.text = user.email
        holder.publish.setOnClickListener {
            onItemSelected(user)
        }
    }

}