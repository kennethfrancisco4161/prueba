package com.prueba.pruebaceiba.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.prueba.pruebaceiba.database.converter.DataConverter
import com.prueba.pruebaceiba.database.dao.PostDao
import com.prueba.pruebaceiba.database.dao.UserDao
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.database.entities.UserRoomEntity

@Database(
    /**
     * Registrar los Entities.
     */
    entities = [
        UserRoomEntity::class,
        PostsRoomEntity::class
    ], version = 2
)
@TypeConverters(DataConverter::class)

abstract class DataBase : RoomDatabase() {
    companion object {
        const val DATABASE_NAME = "data_db"
    }
    /**
     * Registrar los DAOs
     */
    abstract fun userDao(): UserDao
    abstract fun postDao(): PostDao
}