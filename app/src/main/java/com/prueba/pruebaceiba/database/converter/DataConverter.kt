package com.prueba.pruebaceiba.database.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.prueba.pruebaceiba.model.Address
import com.prueba.pruebaceiba.model.Company

class DataConverter {
    /**
     * Convert a a list of Images to a Json
     */
    @TypeConverter
    fun fromAddressJson(stat: Address): String {
        return Gson().toJson(stat)
    }

    /**
     * Convert a json to a list of Images
     */
    @TypeConverter
    fun toAddressObject(jsonImages: String): Address {
        val notesType = object : TypeToken<Address>() {}.type
        return Gson().fromJson<Address>(jsonImages, notesType)
    }

    /**
     * Convert a a list of Images to a Json
     */
    @TypeConverter
    fun fromCompanyJson(stat: Company): String {
        return Gson().toJson(stat)
    }

    /**
     * Convert a json to a list of Images
     */
    @TypeConverter
    fun toCompanyObject(jsonImages: String): Company {
        val notesType = object : TypeToken<Company>() {}.type
        return Gson().fromJson<Company>(jsonImages, notesType)
    }

}