package com.prueba.pruebaceiba.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.prueba.pruebaceiba.database.entities.PostsRoomEntity
import com.prueba.pruebaceiba.database.entities.UserRoomEntity

@Dao
interface PostDao {
    @Transaction
    suspend fun updateData(parameters: List<PostsRoomEntity>) {
        deleteAllParameters()
        insertAll(parameters)
    }

    @Insert
    suspend fun insertAll(parameters: List<PostsRoomEntity>)

    @Query("SELECT * FROM db_posts")
    suspend fun getAllData(): List<PostsRoomEntity>

    @Query("DELETE FROM db_posts")
    suspend fun deleteAllParameters()

    @Query("SELECT * FROM db_posts WHERE userId = :typeProcessId")
    fun getUserByProcessId(typeProcessId: Long): List<PostsRoomEntity>


}