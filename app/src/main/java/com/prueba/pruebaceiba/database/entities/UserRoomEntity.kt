package com.prueba.pruebaceiba.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.prueba.pruebaceiba.model.Address
import com.prueba.pruebaceiba.model.Company

@Entity(tableName = "db_users")
data class UserRoomEntity (
    @PrimaryKey(autoGenerate = false)
    var id: Int,

    @ColumnInfo(name = "name")
    val name:String?,

    @ColumnInfo(name = "username")
    val username:String?,

    @ColumnInfo(name = "email")
    val email:String?,

    @ColumnInfo(name = "address")
    val address: Address?,

    @ColumnInfo(name = "phone")
    val phone: String?,

    @ColumnInfo(name = "website")
    val website: String?,

    @ColumnInfo(name = "company")
    val company: Company?
)
