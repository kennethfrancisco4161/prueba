package com.prueba.pruebaceiba.database.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "db_posts")
data class PostsRoomEntity(
    @PrimaryKey(autoGenerate = false)
    var id: Int,

    @ColumnInfo(name = "userId")
    val userId:Int?,

    @ColumnInfo(name = "title")
    val title:String?,

    @ColumnInfo(name = "body")
    val body:String?,
)
