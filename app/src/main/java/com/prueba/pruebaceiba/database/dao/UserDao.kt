package com.prueba.pruebaceiba.database.dao

import androidx.room.*
import com.prueba.pruebaceiba.database.entities.UserRoomEntity

@Dao
interface UserDao {
    @Transaction
    suspend fun updateData(parameters: List<UserRoomEntity>) {
        deleteAllParameters()
        insertAll(parameters)
    }

    @Insert
    suspend fun insertAll(parameters: List<UserRoomEntity>)

    @Query("SELECT * FROM db_users")
    suspend fun getAllData(): List<UserRoomEntity>

    @Query("DELETE FROM db_users")
    suspend fun deleteAllParameters()

    @Query("SELECT * FROM db_users WHERE id = :typeProcessId LIMIT 1")
    fun getUserByProcessId(typeProcessId: Long): UserRoomEntity

}